import { StarIcon } from "@heroicons/react/solid";
import Image from "next/image";
import { useState } from "react";
import Currency from 'react-currency-formatter';
import { useDispatch } from "react-redux";
import { addToBasket } from "../slices/basketSlice";



const MAX_RATING = 5;
const MIN_RATING = 1;

export default function Product({ title, id, price, description, category, image }) {

    const  dispatch = useDispatch();

    const [rateing] = useState(
        Math.floor(Math.random() * (MAX_RATING - MIN_RATING)) + MIN_RATING
    );

    const [hasPrime] = useState(Math.random() < 0.5);

    const addItemToBasket = () => {
        const product = {
            title, id, price, description, category, image,hasPrime,rateing
        }
        dispatch(addToBasket(product))

    }
    return (
        <div className='relative flex flex-col m-5 bg-white z-30 p-10'>
            <p className=" absolute top-2 right-2 text-xs text-gray-500 ">{category}</p>
            <Image src={image} height={200} width={200} objectFit="contain" />
            <h4 className="my-2">{title}</h4>
            <div className="flex">
                {Array(rateing).fill().map((_, i) => (
                    <StarIcon key={i} className="h-5 text-yellow-500" />
                ))}
            </div>
            <p className="text-xs my-2 line-clamp-2">{description}</p>
            <div className="mb-5">
              <Currency quantity={price} currency="INR" />
            </div>
            {hasPrime && (
                <div className = "flex items-center space-x-2 -mt-5">
                    <img className = 'w-12' src="/images/prime.png" />
                    <p className = "text-xs text-gray-500">FREE Next Day Delivery</p>
                </div>
            )}
            <button onClick={addItemToBasket} className = "mt-auto button">Add To Basket</button>


        </div>
    )
}
