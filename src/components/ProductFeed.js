import Product from "./Product";

export default function ProductFeed({products}) {
    return (
        <div className = "grid grid-flow-row-dense md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 md:-mt-52 mx-auto"> 
            {products.slice(0,4).map(({title,id,price , description , category , image}) =>(
                <Product
                    key={id}
                    id={id}
                    title={title}
                    price={price}
                    description={description}
                    category={category}
                    image={image}
                />

            ))}
            <img src="/images/p1.jpg" className = "md:col-span-full" />
            {products.slice(4).map(({title,id,price , description , category , image}) =>(
                <Product
                    key={id}
                    id={id}
                    title={title}
                    price={price}
                    description={description}
                    category={category}
                    image={image}
                />

            ))}
        </div>
    )
}
